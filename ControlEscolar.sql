SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `ControlEscolar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ControlEscolar` ;

-- -----------------------------------------------------
-- Table `ControlEscolar`.`TIPO_USER`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ControlEscolar`.`TIPO_USER` (
  `cvetipo_user` INT(2) NOT NULL AUTO_INCREMENT,
  `desctipo_user` VARCHAR(128) NULL,
  PRIMARY KEY (`cvetipo_user`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ControlEscolar`.`USER`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ControlEscolar`.`USER` (
  `id` INT(8) NOT NULL AUTO_INCREMENT,
  `cveesc` INT(10) NULL,
  `username` VARCHAR(32) NULL,
  `password` VARCHAR(32) NULL,
  `nombre` VARCHAR(64) NULL,
  `apell_p` VARCHAR(64) NULL,
  `apell_m` VARCHAR(64) NULL,
  `email` VARCHAR(64) NULL,
  `TIPO_USER_cvetipo_user` INT(2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_USER_TIPO_USER_idx` (`TIPO_USER_cvetipo_user` ASC),
  CONSTRAINT `fk_USER_TIPO_USER`
    FOREIGN KEY (`TIPO_USER_cvetipo_user`)
    REFERENCES `ControlEscolar`.`TIPO_USER` (`cvetipo_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `cedicam` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `cedicam` ;

-- -----------------------------------------------------
-- Table `cedicam`.`PROFESOR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cedicam`.`PROFESOR` (
  `cveprofesor` INT(3) NOT NULL AUTO_INCREMENT,
  `cvestatus` INT(3) NULL,
  `nombreprofesor` VARCHAR(64) NULL,
  `ap_profesor` VARCHAR(64) NULL,
  `am_profesor` VARCHAR(64) NULL,
  `ctaprofesor` VARCHAR(32) NULL,
  `passprof` VARCHAR(32) NULL,
  `mail` VARCHAR(128) NULL,
  PRIMARY KEY (`cveprofesor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cedicam`.`ALUMNO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cedicam`.`ALUMNO` (
  `idAlumno` INT(8) NOT NULL AUTO_INCREMENT,
  `nombrealum` VARCHAR(64) NULL,
  `ap_alum` VARCHAR(64) NULL,
  `am_alum` VARCHAR(64) NULL,
  `ctaalum` VARCHAR(32) NULL,
  `passalum` VARCHAR(32) NULL,
  `mail` VARCHAR(128) NULL,
  PRIMARY KEY (`idAlumno`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>SPOT - Free Bootstrap 3 Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  
    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">


    <link href="assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
	<link href="assets/css/style.css" rel='stylesheet' type='text/css' />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,800' rel='stylesheet' type='text/css'>
	<script src="js/jquery-1.9.1.min.js"></script>
	<!--hover-effect-->
	<script src="js/hover_pack.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- CODIGO para activar y desactivar inputs de a cuerdo a un radio button seleccionado -->
    <script language="JavaScript">
	    function habilita(){
	        $(".inputText").removeAttr("disabled");
	    }
	 
	    function deshabilita(){
	        $(".inputText").attr("disabled","disabled");
	    }
	</script>


	<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function()
	{
	    $("input[name=rad2]").click(function () { 

	    	if ($('input:radio[name=rad2]:checked').val() == 'Todas_las_escuelas') {
	    		// 	alert("La edad seleccionada es: " + $('input:radio[name=rad2]:checked').val());
	        	alert("La edad seleccionada es: " + $(this).val());
	    	}
	    	else{
	    		alert("La edad seleccionada es: " + $(this).val());
	    	}   
	        
	    });


	});

    // Procesar

    function procesando()
    {
    	alert("PROCESANDO");
    	debugger;
    	var todasEscuelas = false;
    	var administradores = false;
    	var profesores = false;
    	var alumnos = false;
    	// Identificando activaciones del formulario
    	if ($('input:radio[name=rad2]:checked').val() == 'Todas_las_escuelas')
    		todasEscuelas = true;

    	if ($('input:radio[name=rad]:checked').val() == 'administradores'){
    		administradores = true;
    		alert("Seleccionamos administradores" + administradores);
    	}
    	if ($('input:radio[name=rad]:checked').val() == 'profesores'){
    		profesores = true;
    		alert("Seleccionamos profesores" + profesores);
    	};

    	if ($('input:radio[name=rad]:checked').val() == 'alumnos'){
    		alumnos = true;
    		alert("Seleccionamos alumnos" + alumnos);
    	};
//    	alert("cveesc: " + $("#escu").val());
    	var unicaEscuela = $("#escu").val();
    	var parametros = "todasEscuelas=" + todasEscuelas + "&unicaEscuela=" + unicaEscuela +  "&administradores=" + administradores + "&profesores=" + profesores + "&alumnos=" + alumnos;
    	// Llamas al php con AJAX
    	window.location = "traerinfo.php?" + parametros;
    }

	</script>

	<!-- Script para el editor de texto de correo -->

	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
	  tinymce.init({
	    selector: '#mytextarea'
	  });
  	</script>
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SP<i class="fa fa-circle"></i>T</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="index.html">HOME</a></li>
            <li><a href="about.html">ABOUT</a></li>
            <li><a href="services.html">SERVICES</a></li>
            <li><a href="works.html">WORKS</a></li>
            <li><a data-toggle="modal" data-target="#myModal" href="#myModal"><i class="fa fa-envelope-o"></i></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

	<div id="headerwrap">
		<div class="container">
			<div class="contact" id="contact">
		</div><!-- container -->
	</div><!-- headerwrap -->


	<div id="dg">
		<div class="container">
			<div class="row centered">
				<h4>Seleccionar escuela</h4>
			<form method="post" name='frm' action="contact-post.php">
        		<input type="radio" name="rad2" id="rad2_todas_las_escuelas" value="Todas_las_escuelas" onclick="deshabilita()">
        		Todas las escuelas<br>
        		<input type="radio" name="rad2" id="rad2_escuela" value="Escuela" onclick="habilita()">
        		Escuela<br>
        		<label>Clasificación de la escuela:</label>
        		<!-- <select name='ClasifEscu' disabled class='inputText'> -->
        		<select name="clasif" id="clasif" class='inputText' disabled onchange="buscarEscuela();">
				 	<option value=""> Seleccione una clasificación</option>
					<?php 

						$link = mysql_connect('localhost', 'cad', 'hola123') //Conexion a la BD(servidor,usuario,contraseña)
								or die('No se pudo conectar: ' . mysql_error()); //Mensaje de no conexion
							mysql_select_db('perfiles_db') or die('No se pudo seleccionar la base de datos');//Se selecciona el nombre de la BD

						$sql="SELECT id_clasif, nombre_clasif FROM clasif_esc"; //Pide el atributo nombre_clasif de la tabla clasif_esc
						$rs=mysql_query($sql, $link)or die ("Error: " .mysql_error(). "con el query: " . $sql);//Manda los argumentos del query sql y la conexion a la BD


						//Ciclo donde row es una variable la cual va a guardar un array con los 
						//valores que tiene rs y echo imprime los valores del array en forma de lista
						
						while ($row=mysql_fetch_array($rs)) 
						{ 
							echo "<option value='".$row[0]."'>".$row[1]."</option>"; 
						} 
					?> 
				</select>

        		<label>Escuela:</label>

        		<!-- <select name='Escu' disabled class='inputText'> -->
        		<select name="escu" id="escu" disabled class='inputText' onchange="";>
                    <option value="">Selecciona primero una clasificación</option>
                </select>
			</div>

			<br>
			<br>
			<br>
			<br>

			<div class="row centered">
				<h4>Seleccionar destinatario</h4>
        		<input type="radio" name="rad" value="administradores">
        		Administradores<br>
        		<input type="radio" name="rad" value="profesores">
        		Profesores<br>
        		<input type="radio" name="rad" value="alumnos">
        		Alumnos<br>
        		<input type="radio" name="rad" value="Todos">
        		Todos<br>
			
			</div>

			<div class="row centered">
				<?php echo '<input type="button" name="procesa" id="procesa" onclick="procesando()">' ;?>
				
				<!-- <input type="button" name="procesa" onclick="procesando(.$row->cveesc.');"> -->

<!-- 				onclick="enviaEditar('.$row->id.',\''.$row->nombre.'\',\''.$row->concepto.'\',\''.$row->monto.'\'); -->
			</div>

			<!-- row -->
		</div><!-- container -->
	</div><!-- DG -->


	<!-- FEATURE SECTION -->
	<div class="container">
	  	<div class="row">
  			<br>
  			<br>
  			<br>
  			<h3 class="m_3">Contact</h3>
  			<div class="m_4"></div>

			
				<div class="col-md-6 commentform">
				  <p class="comment-form-author"><label for="author">Name</label>
					<input id="author" name="author" type="text" value="" size="30" aria-required="true">
				  </p>
				  <!--  <p><label for="author">Email</label>
					<input id="author" name="author" type="text" value="" size="30" aria-required="true">
				  </p> -->
				</div>
                <div class="clear"></div>
                <div class="contactform_bottom">
				   <span><label>Subject</label></span>
				   <span><textarea id="mytextarea" name="mytextarea">Hello, World!</textarea></span>
				   <input name="submit" type="submit" id="submit" value="Enviar">
			    </div>
			</form>
	  	</div>
	</div>
	<!-- container -->


	
	
	
	
	
	
	<!-- FOOTER -->
	<div id="f">
		<div class="container">
			<div class="row centered">
				<a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-dribbble"></i></a>
		
			</div><!-- row -->
		</div><!-- container -->
	</div><!-- Footer -->


	<!-- MODAL FOR CONTACT -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">contact us</h4>
	      </div>
	      <div class="modal-body">
		        <div class="row centered">
		        	<p>We are available 24/7, so don't hesitate to contact us.</p>
		        	<p>
		        		Somestreet Ave, 987<br/>
						London, UK.<br/>
						+44 8948-4343<br/>
						hi@blacktie.co
		        	</p>
		        	<div id="mapwrap">
		<iframe height="300" width="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.es/maps?t=m&amp;ie=UTF8&amp;ll=52.752693,22.791016&amp;spn=67.34552,156.972656&amp;z=2&amp;output=embed"></iframe>
					</div>	
		        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Save & Go</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	function cambiaEscuela(){
    		debugger
    		var IdEscuela = 0;
    		IdEscuela = document.getElementById('escu').value;
    		document.getElementById('procesa').setAttribute('onclick','procesando('+IdEscuela+');')
    		//document.getElementById('procesa').value = IdEscuela;
    		
    	}
    </script>
    <script>
		function buscarEscuela(){
				// document.getElementById('clasif').style.display='none';
			var j = jQuery.noConflict();
			// j(document).ready(function(){
				// $("#clasif").change(function(){
				// j("select").change(function(){
					// Vector para saber cuál es el siguiente combo a llenar
					var combos = new Array();
					combos['clasif'] = "escu";
					// Tomo el nombre del combo al que se le a dado el clic
					//posicion = j('#clasif').val();
					valor = j('#clasif').val();
					// Tomo el valor de la opción seleccionada
					//valor = j(this).val()
					posicion='clasif';
					// Evaluó  que si el 'padre' esta seleccionado y el valor es 0, vacíe los 'hijos'
					if(valor==0){
						j("#escu").html('    <option value="0" selected="selected">Seleccione primero el estado</option>')
					}else{
						/* En caso contrario agregado el letreo de cargando a el combo siguiente
							Ejemplo: Si seleccione país voy a tener que el siguiente según mi vector combos es: estado  por qué  combos [país] = estado
						*/
						//j("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
						/* Verificamos si el valor seleccionado es diferente de 0*/
						if(valor!="0"){
							// Llamamos a pagina de combos.php donde ejecuto las consultas para llenar los combos
							j.post("getEscuela.php",{
								combo:j(this).attr("name"), // Nombre del combo
								id:valor // Valor seleccionado
							},function(data){
								j("#"+combos[posicion]).html(data);    //Tomo el resultado de pagina e inserto los datos en el combo indicado                                                                               
							})                                               
						}
					}
				// });
			// });


		}
		
	</script>
  </body>
</html>
